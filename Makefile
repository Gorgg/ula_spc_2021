CC ?= gcc
CFLAGS += -Wall -std=c11 -fPIC

DESTDIR ?= /usr

PKG_CONFIG ?= pkg-config

HUB_INCLUDE = \
	hub/hub.h \
	hub/initial.h \
	hub/ioonly.h \
	hub/active.h \
	hub/record.h
HUB_SRC = \
	hub/main.c \
	hub/initial.c \
	hub/ioonly.c \
	hub/active.c \
	hub/record.c
HUB_OBJ = ${HUB_SRC:.c=.o}

ROVER_INCLUDE = \
	rover/rover.h
ROVER_SRC = \
	rover/main.c
ROVER_OBJ = ${ROVER_SRC:.c=.o}

BASE_INCLUDE = \
	base/base.h
BASE_SRC = \
	base/main.c
BASE_OBJ = ${BASE_SRC:.c=.o}

COMMON_INCLUDE = \
	common/state.h \
	common/protocol.h
COMMON_SRC = \
	common/protocol.c
COMMON_OBJ = ${COMMON_SRC:.c=.o}

LIBEV_CFLAGS :=
LIBEV_LIBS := -lev

WIRINGPI_CFLAGS :=
WIRINGPI_LIBS := -lwiringPi

GSTREAMER_CFLAGS := $(shell ${PKG_CONFIG} --cflags gstreamer-1.0 gstreamer-app-1.0)
GSTREAMER_LIBS := $(shell ${PKG_CONFIG} --libs gstreamer-1.0 gstreamer-app-1.0)

LIBNICE_CFLAGS := $(shell ${PKG_CONFIG} --cflags nice)
LIBNICE_LIBS := $(shell ${PKG_CONFIG} --libs nice)

SDL2_CFLAGS := $(shell ${PKG_CONFIG} --cflags sdl2 SDL2_ttf)
SDL2_LIBS := $(shell ${PKG_CONFIG} --libs sdl2 SDL2_ttf)

.PHONY: all hub base rover install install_hub install_base \
	install_rover clean

hub: hub/hub

base: base/base

rover: rover/rover

all: hub base rover

${HUB_OBJ}:%.o:%.c ${HUB_INCLUDE} ${COMMON_INCLUDE}
	${CC} ${CFLAGS} -Ihub -Icommon -c ${LIBEV_CFLAGS} \
	${GSTREAMER_CFLAGS} ${LIBNICE_CFLAGS} ${WIRINGPI_CFLAGS} -o $@ $<

${BASE_OBJ}:%.o:%.c ${BASE_INCLUDE} ${COMMON_INCLUDE}
	${CC} ${CFLAGS} -Ibase -Icommon -c ${LIBEV_CFLAGS} \
	${GSTREAMER_CFLAGS} ${LIBNICE_CFLAGS} ${SDL2_CFLAGS} -o $@ $<

${ROVER_OBJ}:%.o:%.c ${ROVER_INCLUDE} ${COMMON_INCLUDE}
	${CC} ${CFLAGS} -Irover -Icommon -c ${LIBEV_CFLAGS} \
	${GSTREAMER_CFLAGS} ${LIBNICE_CFLAGS} ${WIRINGPI_CFLAGS} -o $@ $<

${COMMON_OBJ}:%.o:%.c ${COMMON_INCLUDE}
	${CC} ${CFLAGS} -Icommon -c -o $@ $<

hub/hub: ${HUB_OBJ} ${COMMON_OBJ}
	${CC} ${CFLAGS} ${LDFLAGS} -o $@ $< ${LIBEV_LIBS} \
	${GSTREAMER_LIBS} ${LIBNICE_LIBS} ${WIRINGPI_LIBS}

base/base: ${BASE_OBJ} ${COMMON_OBJ}
	${CC} ${CFLAGS} ${LDFLAGS} -o $@ $< ${LIBEV_LIBS} \
	${GSTREAMER_LIBS} ${LIBNICE_LIBS} ${SDL2_LIBS}

rover/rover: ${ROVER_OBJ} ${COMMON_OBJ}
	${CC} ${CFLAGS} ${LDFLAGS} -o $@ $< ${LIBEV_LIBS} \
	${GSTREAMER_LIBS} ${LIBNICE_LIBS} ${WIRINGPI_LIBS}

install: install_hub install_base install_rover

install_hub: hub
	install -D -m755 hub/hub ${DESTDIR}/bin/hub

install_base: base
	install -D -m755 base/base ${DESTDIR}/bin/base
	install -d -D -m644 base/assets/LiberationSans-Regular.ttf \
		${DESTDIR}/share/ula_base/assets/LiberationSans-Regular.ttf

install_rover: rover
	install -D -m755 rover/rover ${DESTDIR}/bin/rover

clean:
	rm -rf hub/hub hub/*.o base/base base/*.o rover/rover rover/*.o \
	common/*.o

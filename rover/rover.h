#include "state.h"

/* This tracks state used by the hub program */
typedef struct rover_inst_struct {
  int retval;

  states cur_state;
  states next_state;

  char streamip[128];
  uint streamport;
  uint hub_fd;

  struct ev_loop *default_loop;

  time_t last_ikimasu;

  int augur_position;
} rover_inst;


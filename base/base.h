/* This tracks data used by the base station program */
typedef struct base_inst_struct {
  int retval;

  states cur_state;
  states next_state;
  states cur_substate;
  states next_substate;

  char streamip[128];
  uint streamport;
  char roverip[128];
  uint roverport;

  struct ev_loop *default_loop;
} base_inst;

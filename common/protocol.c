#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "protocol.h"

int write_message(FILE *fp, prot_message *message) {
  char value_string[VAR_VALUE_MAXLEN];
  bool malformed = false;
  int tempint = message->is_numeric;

  if (message == NULL) {
    fprintf(stderr, "error: write_message: no message data given\n");
    return 1;
  }

  /* Convert numeric values to string values and check correctness in
   * order to avoid putting the data stream into a non-functional state
   */
  if (message->type > MESSAGE_TYPE_ACKNOWLEDGE ||
      message->error > MESSAGE_ERROR_NONEXISTENT_VAR ||
      strnlen(message->var_name, VAR_NAME_MAXLEN) == VAR_NAME_MAXLEN) {
    malformed = true;
  } else if (message->is_numeric) {
    /* See if length will be too long */
    if (snprintf(value_string, 0, "%a", message->var_value.num) + 1 > VAR_VALUE_MAXLEN) {
      malformed = true;
    } else {
      snprintf(value_string, VAR_VALUE_MAXLEN, "%a", message->var_value.num);
    }
  } else {
    if (strnlen(message->var_value.str, VAR_VALUE_MAXLEN) == VAR_VALUE_MAXLEN) {
      /* If there was a null terminator at the very end, strnlen would
       * not include it in the count */
      malformed = true;
    } else {
      strncpy(value_string, message->var_value.str, VAR_VALUE_MAXLEN);
      /* We have already verified that the null terminator will be
       * included */
    }
  }

  if (malformed) {
    fprintf(stderr, "error: write_message: unusable message contents\n");
    return 1;
  }

  /* Write the formatted message */
  if (fprintf(fp, "%d:%d:%s:%s:%d\n", message->type,
                                      message->error,
                                      message->var_name,
                                      value_string,
                                      tempint) < 0) {
    perror("error: write_message: writing failed");
    return 1;
  }

  return 0;
}

int read_message(FILE *fp, prot_message *message) {
  char value_string[VAR_VALUE_MAXLEN];
  bool malformed = false;
  int tempint;

  if (message == NULL) {
    fprintf(stderr, "error: read_message: no message data given\n");
    return 1;
  }

  /* Read the formatted message */
  if (fscanf(fp, "%d:%d:%" STR(VAR_NAME_MAXLEN) "s:%" STR(VAR_VALUE_MAXLEN) "s:%d\n",
             (int *)&message->type,
             (int *)&message->error,
             message->var_name,
             value_string,
             &tempint) != 5) {
    perror("error: read_message: reading/scanning failed");
    return 1;
  }
  if (tempint) {
    message->is_numeric = true;
  } else {
    message->is_numeric = false;
  }

  /* Convert incoming string value to proper format and detect unusable
   * or malformed data */
  if (message->type > MESSAGE_TYPE_ACKNOWLEDGE ||
      message->error > MESSAGE_ERROR_NONEXISTENT_VAR ||
      strnlen(message->var_name, VAR_NAME_MAXLEN) == VAR_NAME_MAXLEN ||
      strnlen(message->var_name, VAR_VALUE_MAXLEN) == VAR_VALUE_MAXLEN) {
    malformed = true;
  } else if (message->is_numeric) {
    if (sscanf(value_string, "%a", &message->var_value.num) != 1) {
      malformed = true;
    }
  } else {
    strncpy(message->var_value.str, value_string, VAR_VALUE_MAXLEN);
  }

  if (malformed) {
    fprintf(stderr, "error: read_message: unusable message contents\n");
    return 1;
  }

  return 0;
}

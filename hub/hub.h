#include "state.h"

/* This tracks state used by the hub program */
typedef struct hub_inst_struct {
  int retval;

  states cur_state;
  states next_state;
  states cur_substate;
  states next_substate;

  char *control_socket;
  char streamip[128];
  uint streamport;
  char roverip[128];
  uint roverport;

  struct ev_loop *default_loop;
} hub_inst;

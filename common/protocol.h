/* Control messages are newline-terminated units consisting of
 * sub-fields separated by colons:
 *
 *     type:error:var_name:var_value:is_numeric\n
 *
 * Control messages are used for most forms of communication between
 * computation units linked by networks, usable for those that
 * manipulate a discrete set of variables and have low data volume
 *
 * Numeric quantities are sent as their printable hexadecimal
 * representations (%a printf format)
 *
 * Each side sets the variable "ikimasu" at a frequency at or less than
 * one message per 10 seconds to indicate it is alive, often at 3
 * seconds. The more intermittent side continually sends these
 * connectivity messages, while the less intermittent side sends them
 * only when it has been verified that the other side is alive, in
 * order to prevent backlogs.
 */

#include <stdio.h>
#include <stdbool.h>

#define STR1(EXP) #EXP
#define STR(EXP) STR1(EXP)

typedef enum message_type_enum {
  MESSAGE_TYPE_SET,          /* Used to set a variable */
  MESSAGE_TYPE_READ,         /* Used to request a variable's value */
  MESSAGE_TYPE_ACKNOWLEDGE,  /* Response to read in order to send back value,
                              * Response to set in order to indicate set success
                              *   Acknowledge-type responses to sets have the same
                              *   field values as the set message except type and
                              *   error
                              */
} message_type;

typedef enum message_error_enum {
  MESSAGE_ERROR_SUCCESS,         /* success */
  MESSAGE_ERROR_ERROR,           /* generic error */
  MESSAGE_ERROR_SET_ERROR,       /* generic error resulting from set operation */
  MESSAGE_ERROR_NONEXISTENT_VAR, /* communicated non-existence of variable in current state in acknowledge */
} message_error;

/* Maxiumum number of characters in variable name, including null
 * terminator */
#define VAR_NAME_MAXLEN 32

/* Maxiumum number of characters in variable value, including null
 * terminator */
#define VAR_VALUE_MAXLEN 128

typedef union var_value_union {
  char str[VAR_VALUE_MAXLEN];
  float num;
} var_value_t;

typedef struct prot_message_struct {
  message_type type;                /* type of message */
  message_error error;              /* indicates if error */
  char var_name[VAR_NAME_MAXLEN];   /* zero-terminated string with name of target variable */
  var_value_t var_value;            /* zero-terminated string with value of target variable
                                     * or number with value to be converted to string, depending
                                     * on value of is_numeric */
  bool is_numeric;
} prot_message;

/* Write a message to an fd, blocking, with contents specified by
 * a prot_message struct
 * retval: 0 on success, other otherwise */
int write_message(FILE *fp, prot_message *message);

/* Read a message from an fd, blocking, with contents set into a
 * prot_message struct
 * retval: 0 on success, other otherwise */
int read_message(FILE *fp, prot_message *message);

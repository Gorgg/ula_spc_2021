#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <threads.h>
#include <time.h>
#include <ev.h>

#include "protocol.h"
#include "state.h"

/* varnames:
 * left
 * right
 * aug_position
 * augur */

typedef struct ioonly_data_struct {
  rover_inst *rover;
  int retval;
} ioonly_data;

void standby_stdin_cb(EV_P_ ev_io *w, int revents) {
  ioonly_data *data = (ioonly_data *)w->data;
  prot_message message;
  time_t curtime;

  if (read_message(stdin, &message) != 0) {
    fprintf(stderr, "error: could not read message from stdin or malformed message\n");
    return;
  }

  if (message.error != MESSAGE_ERROR_SUCCESS) {
    fprintf(stderr, "error: received message with embedded error, not processing\n");
    return;
  }

  switch (message.type) {
    case MESSAGE_TYPE_ACKNOWLEDGE:
      if (strncmp("ikimasu", message.var_name, 8) == 0) {
        curtime = time(NULL);
        if (curtime == ((time_t) -1)) {
          perror("time");
          retval = 1;
          ev_break(EV_A_ EVBREAK_ONE);
        }
        data->rover->last_ikimasu = curtime;
      }
      return;
    case MESSAGE_TYPE_READ:
      if (strncmp("left", message.var_name, VAR_NAME_MAXLEN) == 0) {
      } else if (strncmp("right", message.var_name, VAR_NAME_MAXLEN) == 0) {
      } else if (strncmp("augur", message.var_name, VAR_NAME_MAXLEN) == 0) {
      } else if (strncmp("aug_position", message.var_name, VAR_NAME_MAXLEN) == 0) {
      } else {
        fprintf(stderr, "error: unrecognized variable name requested for read, possibly malformed\n");
        return;
      }

      break; /* requires further processing */
    case MESSAGE_TYPE_SET:
      if (message.is_numeric == false) {
        fprintf(stderr, "error: improper encapsulated data type of message, discarded\n");
        return;
      }

      if (strncmp("ikimasu", message.var_name, VAR_NAME_MAXLEN) == 0) {
        curtime = time(NULL);
        if (curtime == ((time_t) -1)) {
          perror("time");
          retval = 1;
          ev_break(EV_A_ EVBREAK_ONE);
        }
        data->rover->last_ikimasu = curtime;
      } else if (strncmp("left", message.var_name, VAR_NAME_MAXLEN) == 0) {
      } else if (strncmp("right", message.var_name, VAR_NAME_MAXLEN) == 0) {
      } else if (strncmp("augur", message.var_name, VAR_NAME_MAXLEN) == 0) {
      } else if (strncmp("aug_position", message.var_name, VAR_NAME_MAXLEN) == 0) {
      } else {
        fprintf(stderr, "error: unrecognized message name requested for setting, possibly malformed\n");
        return;
      }

      break; /* requires further processing */
    default:
      fprintf(stderr, "error: unrecognized message type received\n");
      retval = 1;
      ev_break(EV_A_ EVBREAK_ONE);
      break;
  }

  if (write_message(stdout, &message) != 0) {
    fprintf(stderr, "error: could not acknowledge message to stdin\n");
    retval = 1;
    ev_break(EV_A_ EVBREAK_ONE);
  }
}

void standby_ikiru_cb(EV_P_ ev_timer *w, int revents) {
  ioonly_data *data = (ioonly_data *)w->data;
  prot_message message;
  time_t curtime;

  message.type = MESSAGE_TYPE_SET;
  message.error = MESSAGE_ERROR_SUCCESS;
  message.var_name = "ikimasu";
  message.var_value.num = 1;
  message.is_numeric = true;

  if (write_message(stdout, &message) != 0) {
    fprintf(stderr, "error: could not send ikiru message\n");
    retval = 1;
    ev_break(EV_A_ EVBREAK_ONE);
    return;
  }

  curtime = time(NULL);
  if (curtime == ((time_t) -1)) {
    perror("time");
    retval = 1;
    ev_break(EV_A_ EVBREAK_ONE);
  }

  if (curtime - data->rover->last_ikimasu > 10) {
    fprintf(stderr, "note: communications timeout, locking motion\n");
    // lock motors
  }
}

int run_ioonly(rover_inst *rover) {
  ev_io stdin_watcher;
  ev_timer ikiru_watcher;
  ioonly_data data;
  data.retval = 0;

  ev_io_init(&stdin_watcher, ioonly_stdin_cb, STDIN_FILENO, EV_READ);
  stdin_watcher.data = data;
  ev_io_start(rover.default_loop, &stdin_watcher);

  ev_timer_init(&ikiru_watcher, ioonly_ikiru_cb, 0, 3);
  ikiru_watcher.data = data;
  ev_timer_start(rover.default_loop,

  ev_run(rover.default_loop, 0);

  ev_io_stop(rover.default_loop, &stdin_watcher);
  ev_timer_stop(rover.default_loop, &ikiru_watcher);

  return data.retval;
}

#include <stdio.h>
#include <stdbool.h>
#include <fcntl.h>
#include <threads.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <ev.h>
#include <gst/gst.h>
#include <gst/app/gstappsink.h>

#include "protocol.h"
#include "state.h"

#define ASSETS_PATH /usr/share/ula_base/assets

const SDL_Color color_black =           {   0,   0,   0, 255 };
const SDL_Color color_white =           { 255, 255, 255, 255 };
const SDL_Color color_target_green =    {   0, 255,   0, 255 };
const SDL_Color color_active_blue =     { 127, 127, 255, 255 };
const SDL_Color color_inert_grey =      { 100, 100, 100, 255 };
const SDL_Color color_state_yellow =    { 127, 127,   0, 255 };
const SDL_Color color_curstate_orange = { 255, 128,   0, 255 };

typedef struct io_async_data_struct {
  mtx_t mutex;
  bool terminate;
  ev_async watcher;

} io_async_data;

typedef struct textrect_struct {
  char *text;
  int x;
  int y;
  int w;
  int h;
  SDL_Color fill_color;
  SDL_Color border_color;
  /* Must acquire mutex before setting val! */
  bool *associated_triggerval;
  bool needs_update;
} textrect;

typedef enum textrect_index_enum {
  TEXTRECT_ROVER_ALIVE,
  TEXTRECT_ROVER_IOONLY,
  TEXTRECT_ROVER_STANDBY,
  TEXTRECT_ROVER_ACTIVE,
  TEXTRECT_ROVER_AUGURPOSITION,
  TEXTRECT_HUB_ALIVE,
  TEXTRECT_HUB_MOISTURE,
  TEXTRECT_HUB_METAL,
  TEXTRECT_HUB_IMU,
  TEXTRECT_HUB_GPS,
  TEXTRECT_HUB_IOONLY,
  TEXTRECT_HUB_ACTIVE,
  TEXTRECT_HUB_INITIAL,
  TEXTRECT_HUB_ASCENT,
  TEXTRECT_HUB_DESCENT,
} textrect_index;
#define NUM_TEXTRECS 15

void help() {
  fprintf(stderr, "Usage: base [hub_socket] [hub_videoip] [rover_socket] [rover_videoip]\n"
                  "Example: base /run/hub 1.2.3.4:5678 /run/rover 8.7.6.5:4321\n");
}

int video_routine (textrect *textrects) {
}

typedef struct io_routine_args_struct {
  int hub_fd, rover_fd;
  bool *io_thrd_exit_cond, *rects_new_data;
  struct ev_loop *default_loop;
  textrect *textrects;
  io_async_data async_data;
} io_routine_args;

typedef struct io_cb_args_struct {
  io_routine_args *args;
  int retval;
} io_cb_args;

void io_async_cb(EV_P_ ev_async *w, int revents) {
  io_cb_args *cb_args = (io_cb_args *)(&w->data);

  if (mtx_lock(&cb_args->args->async_data.mutex) != thrd_success) {
    fprintf(stderr, "error: io thread async callback could not acquire mutex\n");
    cb_args->retval = 1;
    ev_break(EV_A_ EVBREAK_ONE);
  }

  if (cb_args->args->async_data.terminate) {
    ev_break(EV_A_ EVBREAK_ONE);
  }

  /* Process keypresses/button presses */

  if (mtx_unlock(&cb_args->args->async_data.mutex) != thrd_success) {
    fprintf(stderr, "error: io thread async callback could not release mutex\n");
    cb_args->retval = 1;
    ev_break(EV_A_ EVBREAK_ONE);
  }
}

void io_hub_cb (EV_P_ ev_async *w, int revents) {
}

void io_rover_cb (EV_P_ ev_async *w, int revents) {
}

void io_connection_cb (EV_P_ ev_async *w, int revents) {
}

int io_routine (io_routine_args *args) {
  io_cb_args cb_args = {args, 0};
  ev_io hub_watcher, rover_watcher;
  ev_periodic connection_watcher;

  *args->io_thrd_exit_cond = true;
  return cb_args.retval;
}

int main (int argc, char **argv) {
  int retval = 0;
  SDL_Window *window = NULL;
  SDL_Renderer *renderer = NULL;
  SDL_Surface *window_surface = NULL;
  int hub_fd = -1, rover_fd = -1;
  char hub_videoip[128], rover_videoip[128];
  unsigned int hub_videoport, rover_videoport;
  TTF_Font *font = NULL;
  struct ev_loop *default_loop;
  thrd_t io_thrd;
  bool io_thrd_exit_cond = false, rects_new_data = true; /* Used to signal between threads, video thread polls */
  io_routine_args io_args;
  io_async_data async_data;
  int io_result;

  if (argc != 5) {
    if (argc == 1) {
      if (strncmp("-h", argv[1], 3) == 0) {
        retval = 0;
      } else {
        retval = 1;
      }
    } else {
      retval = 1;
    }
    help();
    return retval;
  }

  if (sscanf(argv[2], "%127s:%d", rover_videoip, &rover_videoport) != 2) {
    perror("sscanf");
    return 1;
  }
  if (sscanf(argv[4], "%127s:%d", hub_videoip, &hub_videoport) != 2) {
    perror("sscanf");
    return 1;
  }

  hub_fd = open(argv[1], O_RDWR);
  if (hub_fd < 0) {
    perror("open");
    return 1;
  }
  rover_fd = open(argv[3], O_RDWR);
  if (rover_fd < 0) {
    perror("open");
    return 1;
  }

  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    fprintf(stderr, "error: could not initialize SDL: %s\n", SDL_GetError());
    return 1;
  }

  window = SDL_CreateWindow("Base Station Interface",
                            SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED,
                            500, 400,
                            SDL_WINDOW_SHOWN);
  if (window == NULL) {
    fprintf(stderr, "error: could not create SDL window: %s\n", SDL_GetError());
    retval = 1;
    goto cleanup_one;
  }

  renderer = SDL_CreateRenderer(window, -1, 0);
  if (renderer == NULL) {
    fprintf(stderr, "error: could not create SDL renderer: %s\n", SDL_GetError());
    retval = 1;
    goto cleanup_one;
  }

  if (TTF_Init() != 0) {
    fprintf(stderr, "error: could not initialize SDL ttf library\n");
    retval = 1;
    goto cleanup_one;
  }

  font = TTF_OpenFont(STR(ASSETS_PATH) "/LiberationSans-Regular.ttf", 16);
  if (!font) {
    fprintf(stderr, "error: could not open font LiberationSans-Regular.ttf, expected at: %s\n",
                    STR(ASSETS_PATH) "/LiberationSans-Regular.ttf");
    retval = 1;
    goto cleanup_two;
  }

  gst_init(&argc, &argv);

  default_loop = ev_default_loop(0);
  if (!default_loop) {
    fprintf(stderr, "error: failed to acquire default loop\n");
    retval = 1;
    goto cleanup_two;
  }

  window_surface = SDL_GetWindowSurface(window);
  if (window_surface == NULL) {
    fprintf(stderr, "error: could not get SDL window surface: %s\n", SDL_GetError());
    retval = 1;
    goto cleanup_two;
  }

  if (SDL_FillRect(window_surface, NULL,
      SDL_MapRGB(window_surface->format, 0x00, 0x00, 0x35)) != 0) {
    fprintf(stderr, "error: could not fill SDL window surface with background color: %s\n", SDL_GetError());
    retval = 1;
    goto cleanup_two;
  }

  if (SDL_UpdateWindowSurface(window) != 0) {
    fprintf(stderr, "error: could not update SDL window for background color: %s\n", SDL_GetError());
    retval = 1;
    goto cleanup_two;
  }

  if (mtx_init(&async_data.mutex, mtx_plain) != thrd_success) {
    fprintf(stderr, "error: could not initialize mutext for io asynchronous data\n");
    retval = 1;
    goto cleanup_two;
  }

  textrect textrects[NUM_TEXTRECS] = {
    { "Rover Control Not Present", 12, 412, 476, 76, color_inert_grey, color_black, NULL, true }, /* TEXTRECT_ROVER_ALIVE */
    { "I/O Only", 344, 512, 142, 76, color_state_yellow, color_black, NULL, true }, /* TEXTRECT_ROVER_IOONLY */
    { "Standby", 12, 512, 142, 76, color_state_yellow, color_black, NULL, true }, /* TEXTRECT_ROVER_STANDBY */
    { "Roving", 178, 512, 142, 76, color_state_yellow, color_black, NULL, true }, /* TEXTRECT_ROVER_ACTIVE */
    { "Placeholder", 12, 512, 101, 76, color_white, color_black, NULL, true }, /* TEXTRECT_ROVER_AUGURPOSITION */
    { "Rover Control Not Present", 12, 412, 476, 76, color_inert_grey, color_black, NULL, true }, /* TEXTRECT_HUB_ALIVE */
    { "Placeholder", 512, 612, 101, 76, color_white, color_black, NULL, true }, /* TEXTRECT_HUB_MOISTURE */
    { "Placeholder", 637, 612, 101, 76, color_white, color_black, NULL, true }, /* TEXTRECT_HUB_METAL */
    { "Placeholder", 762, 612, 101, 76, color_white, color_black, NULL, true }, /* TEXTRECT_HUB_IMU */
    { "Placeholder", 887, 612, 101, 76, color_white, color_black, NULL, true }, /* TEXTRECT_HUB_GPS */
    { "I/O Only", 344, 512, 142, 76, color_state_yellow, color_black, NULL, true }, /* TEXTRECT_HUB_IOONLY */
    { "Hubbing", 812, 512, 76, 76, color_state_yellow, color_black, NULL, true }, /* TEXTRECT_HUB_ACTIVE */
    { "Standby", 512, 512, 76, 76, color_state_yellow, color_black, NULL, true }, /* TEXTRECT_HUB_INITIAL */
    { "Ascending", 612, 512, 76, 76, color_state_yellow, color_black, NULL, true }, /* TEXTRECT_HUB_ASCENT */
    { "Ejected", 712, 512, 76, 76, color_state_yellow, color_black, NULL, true }, /* TEXTRECT_HUB_DESCENT */
  };

  io_args.hub_fd = hub_fd;
  io_args.rover_fd = rover_fd;
  io_args.io_thrd_exit_cond = &io_thrd_exit_cond;
  io_args.rects_new_data = &rects_new_data;
  io_args.default_loop = default_loop;
  io_args.textrects = &textrects;
  ev_async_init(&io_args.async_data.watcher, io_async_cb);
  if (thrd_create(&io_thrd, (thrd_start_t)(*io_routine), &io_args) != thrd_success) {
    fprintf(stderr, "error: could not create io thread\n");
    retval = 1;
    goto cleanup_three;
  }



cleanup_four:
  if (thrd_join(io_thrd, &io_result) != thrd_success) {
    fprintf(stderr, "error: could not join io thread\n");
    retval = 1;
  }

cleanup_three:
  mtx_destroy(&async_data.mutex);

cleanup_two:
  if (font) {
    TTF_CloseFont(font);
  }
  TTF_Quit();

cleanup_one:
  if (renderer) {
    SDL_DestroyRenderer(renderer);
  }
  if (window) {
    SDL_DestroyWindow(window);
  }
  SDL_Quit();

  return retval;
}

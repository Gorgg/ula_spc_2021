#include <stdio.h>
#include <threads.h>
#include <string.h>
#include <ev.h>
#include <gst/gst.h>

#include "state.h"
#include "protocol.h"
#include "hub.h"
#include "initial.h"
#include "hub_ioonly.h"
#include "hub_active.h"
#include "record.h"

const struct statestring {
  char *str;
  states state;
  states sub_state;
} statestrings[6] = {
  {"io_only",        STATE_IOONLY,  STATE_NONE},
  {"record_ascent",  STATE_RECORD,  SUBSTATE_RECORD_ASCENT},
  {"record_descent", STATE_RECORD,  SUBSTATE_RECORD_DESCENT},
  {"initial",        STATE_INITIAL, STATE_NONE},
  {"active",         STATE_ACTIVE,  STATE_NONE},
  {"exit",           STATE_EXIT,    STATE_NONE),
}
#define NUM_STATESTRINGS 6

void help() {
  fprintf(stderr, "Usage: hub [initstate] [control_socket] [streamip] [rover_listenfile]\n"
                  "Example: hub initial /run/control 1.2.3.4:5678 /tmp/roverconnect\n");
}

int main (int argc, char **argv) {
  hub_inst hub;
  hub.retval = 0;
  int state_retval;
  char ip_temp[128];

  if (argc != 5) {
    if (argc == 1) {
      if (strncmp("-h", argv[1], 3) == 0) {
        hub.retval = 0;
      } else {
        hub.retval = 1;
      }
    } else {
      hub.retval = 1;
    }
    help();
    return hub.retval;
  }

  hub.cur_state = STATE_NONE;
  hub.cur_substate = STATE_NONE;
  hub.next_state = STATE_NONE;
  hub.next_substate = STATE_NONE;
  for (int i = 0; i < NUM_STATESTRINGS, i++) {
    if(strncmp(statestrings[i].str, argv[1], 15) == 0) {
      hub.next_state = statestrings[i].state;
      hub.next_substate = statestrings[i].sub_state;
      break;
    }
  }
  if (hub.next_state == STATE_NONE) {
    fprintf(stderr, "error: could not recognize specified initial state\n");
    return 1;
  }

  if (sscanf(argv[3], "%127s:%d", hub.streamip, &hub.streamport) != 2)
    perror("sscanf");
    return 1;
  }
  if (sscanf(argv[4], "%127s:%d", hub.roverip, &hub.roverport) != 2)
    perror("sscanf");
    return 1;
  }

  control_socket = argv[2];

  gst_init(&argc, &argv);

  hub.default_loop = ev_default_loop(0);
  if (!hub.default_loop) {
    fprintf(stderr, "error: failed to acquire default loop\n");
    return 1;
  }

  /* Enter and facilitate transition between states */
  while (hub.next_state != STATE_EXIT) {
    switch (hub.next_state) {
      case STATE_INITIAL:
        fprintf(stderr, "note: entering initial state\n");
        state_retval = run_initial(&hub);
        if (state_retval != 0) {
          fprintf(stderr, "warning: run of state initial exited with value %d\n", state_retval);
        }
        break;
      case STATE_RECORD:
        fprintf(stderr, "note: entering record state\n");
        state_retval = run_record(&hub);
        if (state_retval != 0) {
          fprintf(stderr, "warning: run of state record exited with value %d\n", state_retval);
        }
        break;
      case STATE_ACTIVE:
        fprintf(stderr, "note: entering active state\n");
        state_retval = run_active(&hub);
        if (state_retval != 0) {
          fprintf(stderr, "warning: run of state active exited with value %d\n", state_retval);
        }
        break;
      case STATE_IOONLY:
        fprintf(stderr, "note: entering ioonly state\n");
        state_retval = run_ioonly(&hub);
        if (state_retval != 0) {
          fprintf(stderr, "warning: run of state ioonly exited with value %d\n", state_retval);
        }
        break;
      default:
        fprintf(stderr, "error: unrecognized state index: %d\n", hub.next_state);
        return 1;
    }
  }
  fprintf(stderr, "note: entering exit state\n");

  return retval;
}

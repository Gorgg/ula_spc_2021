#include <stdio.h>
#include <unistd.h>
#include <ev.h>

#include "rover.h"
#include "protocol.h"
#include "state.h"

typedef struct standby_data_struct {
  rover_inst *rover;
  int retval;
} standby_data;

void standby_hub_cb(EV_P_ ev_io *w, int revents) {
  standby_data *data = (standby_data *)w->data;
  char tmpbuf;

  /* We have been woken up from standby by the hub */
  if (read(STDIN_FILENO, &tmpbuf, 1) != 0) {
    perror("read");
    data->retval = 1;
  }

  data->rover->next_state = STATE_ACTIVE;

  ev_break(EV_A_ EVBREAK_ONE);
}

int run_standby(rover_inst *rover) {
  ev_io hub_watcher;
  standby_data data;
  data.retval = 0;

  ev_io_init(&stdin_watcher, standby_stdin_cb, STDIN_FILENO, EV_READ);
  stdin_watcher.data = data;
  ev_io_start(rover.default_loop, &stdin_watcher);

  ev_run(rover.default_loop, 0);

  ev_io_stop(rover.default_loop, &hub_watcher);

  return data.retval;
}

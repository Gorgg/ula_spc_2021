#include <stdio.h>
#include <threads.h>
#include <fcntl.h>
#include <ev.h>
#include <gst/gst.h>

#include "state.h"
#include "protocol.h"
#include "rover.h"
#include "hub_active.h"
#include "hub_ioonly.h"
#include "standby.h"

void help() {
  fprintf(stderr, "Usage: rover [initstate] [streamip] [hub_listenfile]\n"
                  "Example: rover standby 1.2.3.4:5678 /tmp/hubconnect\n");
}

const struct statestring {
  char *str;
  states state;
} statestrings[6] = {
  {"io_only",        STATE_IOONLY},
  {"standby",        STATE_STANDBY},
  {"active",         STATE_ACTIVE},
}
#define NUM_STATESTRINGS 3

int main(int argc, char **argv) {
  rover_inst rover;
  rover.retval = 0;

  if (argc != 3) {
    if (argc == 1) {
      if (strncmp("-h", argv[1], 3) == 0) {
        rover.retval = 0;
      } else {
        rover.retval = 1;
      }
    } else {
      rover.retval = 1;
    }
    help();
    return rover.retval;
  }

  rover.cur_state = STATE_NONE;
  rover.next_state = STATE_NONE;
  for (int i = 0; i < NUM_STATESTRINGS, i++) {
    if(strncmp(statestrings[i].str, argv[1], 15) == 0) {
      rover.next_state = statestrings[i].state;
      break;
    }
  }
  if (rover.next_state == STATE_NONE) {
    fprintf(stderr, "error: could not recognize specified initial state\n");
    return 1;
  }

  if (sscanf(argv[2], "%127s:%d", rover.streamip, &rover.streamport) != 2)
    perror("sscanf");
    return 1;
  }

  rover.hub_fd = open(argv[3], O_RDWR);
  if (rover.hub_fd < 0) {
    perror("open");
    return 1;
  }

  gst_init(&argc, &argv);

  rover.default_loop = ev_default_loop(0);
  if (!rover.default_loop) {
    fprintf(stderr, "error: failed to acquire default loop\n");
    return 1;
  }

  /* Enter and facilitate transition between states */
  while (rover.next_state != STATE_EXIT) {
    switch (rover.next_state) {
      case STATE_STANDBY:
        fprintf(stderr, "note: entering standby state\n");
        state_retval = run_standby(&rover);
        if (state_retval != 0) {
          fprintf(stderr, "warning: run of state standby exited with value %d\n", state_retval);
        }
        break;
      case STATE_ACTIVE:
        fprintf(stderr, "note: entering active state\n");
        state_retval = run_active(&rover);
        if (state_retval != 0) {
          fprintf(stderr, "warning: run of state active exited with value %d\n", state_retval);
        }
        break;
      case STATE_IOONLY:
        fprintf(stderr, "note: entering ioonly state\n");
        state_retval = run_ioonly(&rover);
        if (state_retval != 0) {
          fprintf(stderr, "warning: run of state ioonly exited with value %d\n", state_retval);
        }
        break;
      default:
        fprintf(stderr, "error: unrecognized state index: %d\n", rover.next_state);
        return 1;
    }
  }
  fprintf(stderr, "note: entering exit state\n");

  return retval;
}

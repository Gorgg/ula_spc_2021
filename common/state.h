/* The states a given unit may be in */
typedef enum states_enum {
  STATE_NONE,              /* rover, hub: used to indicated no state */
  STATE_IOONLY,            /* rover, hub: process commands only */
  STATE_EXIT,              /* rover, hub: used to represent program termination */
  STATE_RECORD,            /* hub: during ascent/descent, recording */
  STATE_INITIAL,           /* hub: before rocket ascent, not transmitting */
  STATE_ACTIVE,            /* hub: main mission loop */
  STATE_STANDBY,           /* rover: standby (possibly will go unused) */
  STATE_CONTROL,           /* rover: main mission loop */
  SUBSTATE_RECORD_ASCENT,  /* hub: during ascent, attached to rocket,
                            * recording and (CRITICALLY) not transmitting */
  SUBSTATE_RECORD_DESCENT, /* hub: during descent, detached from rocket,
                            * recording and transmitting */
} states;
